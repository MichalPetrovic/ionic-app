angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('menu.home', {
    url: '/home',
    views: {
      'side-menu21': {
        templateUrl: 'templates/home.html',
        controller: 'homeCtrl'
      }
    }
  })

  .state('menu.photos', {
    url: '/photos',
    views: {
      'side-menu21': {
        templateUrl: 'templates/photos.html',
        controller: 'photosCtrl'
      }
    }
  })

  .state('menu', {
    url: '/side-menu21',
    templateUrl: 'templates/menu.html',
    controller: 'menuCtrl'
  })

  .state('menu.logIn', {
    url: '/login',
    views: {
      'side-menu21': {
        templateUrl: 'templates/logIn.html',
        controller: 'logInCtrl'
      }
    }
  })

  .state('menu.signUp', {
    url: '/signup',
    views: {
      'side-menu21': {
        templateUrl: 'templates/signUp.html',
        controller: 'signUpCtrl'
      }
    }
  })

  .state('menu.succesffulLogIn', {
    url: '/afterlogin',
    views: {
      'side-menu21': {
        templateUrl: 'templates/succesffulLogIn.html',
        controller: 'succesffulLogInCtrl'
      }
    }
  })

$urlRouterProvider.otherwise('/side-menu21/home')

  

});